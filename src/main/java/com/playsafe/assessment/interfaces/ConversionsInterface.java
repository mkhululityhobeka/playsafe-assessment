package com.playsafe.assessment.interfaces;

import com.playsafe.assessment.model.FromCelsiusToKelvinsDTO;
import com.playsafe.assessment.model.FromKelvinsToCelsiusDTO;
import com.playsafe.assessment.model.MilesToKilometers;
import com.playsafe.assessment.model.kilometersToMilesDTO;


public interface ConversionsInterface {

	public double returnCelsius(FromKelvinsToCelsiusDTO kelvins);
	public double returnkelvinds(FromCelsiusToKelvinsDTO celsius);
	public double returnKilometers(MilesToKilometers miles);
	public double returnMiles(kilometersToMilesDTO  kilometers);
}
