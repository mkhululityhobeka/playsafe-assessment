package com.playsafe.assessment.services;

import org.springframework.stereotype.Service;
import com.playsafe.assessment.interfaces.ConversionsInterface;
import com.playsafe.assessment.model.FromCelsiusToKelvinsDTO;
import com.playsafe.assessment.model.FromKelvinsToCelsiusDTO;
import com.playsafe.assessment.model.MilesToKilometers;
import com.playsafe.assessment.model.kilometersToMilesDTO;



@Service
public class ConvertionsService implements ConversionsInterface{

	
	/* method to convert Kelvins to Celsius i.e returning temperature in celsius* */
	@Override
	public double returnCelsius(FromKelvinsToCelsiusDTO kelvins) {
			   double celsius = kelvins.getKelvin() - 273.15;
		       return celsius;
	}


	/* method to convert Celsius  to Kelvins  i.e returning temperature in Kelvins* */
	@Override
	public double returnkelvinds(FromCelsiusToKelvinsDTO celsius) {
		      double kelvins = celsius.getCelsius() +  273.15;
		      return kelvins;
	}


	/* method to convert Miles  to Kilometers  i.e returning Kilometers* */
	@Override
	public double returnKilometers(MilesToKilometers miles) {
			double milesToKm = miles.getMiles()*1.609344;
			return milesToKm;
	}


	/* method to convert Kilometers  to Miles  i.e returning Miles* */
	@Override
	public double returnMiles(kilometersToMilesDTO kilometers) {
			double kilometersToMiles = kilometers.getKilometers() * 0.62137;
			return kilometersToMiles;
	}

}
