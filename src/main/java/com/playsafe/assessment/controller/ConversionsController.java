package com.playsafe.assessment.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.playsafe.assessment.model.FromCelsiusToKelvinsDTO;
import com.playsafe.assessment.model.FromKelvinsToCelsiusDTO;
import com.playsafe.assessment.model.MilesToKilometers;
import com.playsafe.assessment.model.kilometersToMilesDTO;
import com.playsafe.assessment.services.ConvertionsService;


@RestController
@RequestMapping("/conversions")
public class ConversionsController {
	
	
			@Autowired 
			ConvertionsService conversionService;
				
			
			@PostMapping("ktoc")/* http://locahost:8080/conversions/ktoc endpoint i.epass FromKelvinsToCelsiusDTO object  **/
			public ResponseEntity<String>convertKelvinsToCelsius(@RequestBody FromKelvinsToCelsiusDTO  kelvins){
		
				double kelvinsToCelsius =  conversionService.returnCelsius(kelvins);
				
				return new ResponseEntity<String>( String.valueOf(kelvinsToCelsius)+"C",HttpStatus.OK);
			}
			
			
			@PostMapping("ctok")/* http://locahost:8080/conversions/ctok endpoint  i.e pass FromCelsiusToKelvinsDTO object **/
			public ResponseEntity<String>convertCelsiusToKelvins(@RequestBody FromCelsiusToKelvinsDTO  fromCelsiusToKelvinsDTO){
		
				double celsiusToKelvins =  conversionService.returnkelvinds(fromCelsiusToKelvinsDTO);
				
				return new ResponseEntity<String>( String.valueOf(celsiusToKelvins)+"K",HttpStatus.OK);
			}
			
			
			@PostMapping("mtok")/* http://locahost:8080/conversions/mtok endpoint  i.e pass MilesToKilometers object**/
			public ResponseEntity<String>convertMilesToKm(@RequestBody MilesToKilometers  MilesToKilometers){
		
				double milesToKilometers=  conversionService.returnKilometers(MilesToKilometers);
				
				return new ResponseEntity<String>( String.valueOf(milesToKilometers)+"km",HttpStatus.OK);
			}
			
			
			@PostMapping("ktom")/* http://locahost:8080/conversions/ktom endpoint i.e  pass kilometersToMilesDTO object* */
			public ResponseEntity<String>convertKilometersToMeters(@RequestBody kilometersToMilesDTO   kilometersToMilesDTO){
		
				double kilometersToMiles=  conversionService.returnMiles(kilometersToMilesDTO);
				
				return new ResponseEntity<String>( String.valueOf(kilometersToMiles)+"mi",HttpStatus.OK);
			}
			

}
