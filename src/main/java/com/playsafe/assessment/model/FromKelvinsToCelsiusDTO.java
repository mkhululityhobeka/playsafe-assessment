package com.playsafe.assessment.model;

import lombok.Data;



@Data
public class FromKelvinsToCelsiusDTO {
	
	private double kelvin;
	
}
