package com.playsafe.assessment.model;

import lombok.Data;

@Data
public class FromCelsiusToKelvinsDTO {
	
	private double celsius;

}
