package com.playsafe.assessment.model;

import lombok.Data;

@Data
public class kilometersToMilesDTO {
	
	private double kilometers;

}
