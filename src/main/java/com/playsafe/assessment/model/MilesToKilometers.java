package com.playsafe.assessment.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class MilesToKilometers {
	
	private double miles;

}
